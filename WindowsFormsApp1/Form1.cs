﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Double A;
        public Double B;
        public int operacija=0;
        public Form1()
        {
            InitializeComponent();
        }
        private void Izracunaj()
        {
            if (operacija == 1)
            {
                label1.Text = (A + B).ToString();
            }
            if (operacija == 2)
            {
                label1.Text = (A - B).ToString();
            }
            if (operacija == 3)
            {
                label1.Text = (A * B).ToString();
            }
            if (operacija == 4)
            {
                label1.Text = (A / B).ToString();
            }
            if (operacija == 5)
            {
                label1.Text = (Math.Sin(B*Math.PI/180).ToString());
            }
            if (operacija == 6)
            {
                label1.Text = (Math.Cos(B * Math.PI / 180).ToString());
            }
            if (operacija == 7)
            {
                label1.Text = (Math.Log(B).ToString());
            }
            if (operacija == 8)
            {
                label1.Text = (Math.Log10(B).ToString());
            }
            if (operacija == 9)
            {
                label1.Text = (Math.Pow(A,B).ToString());
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Double.TryParse(textBox1.Text, out A);
            operacija = 2;
            textBox1.Clear();
            label1.Text = A.ToString() + "-";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Double.TryParse(textBox1.Text, out A);
            operacija = 3;
            textBox1.Clear();
            label1.Text = A.ToString() + "*";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Double.TryParse(textBox1.Text, out A);
            operacija = 4;
            textBox1.Clear();
            label1.Text = A.ToString() + "/";
        }
        private void button4_Click(object sender, EventArgs e)
        {
            label1.Text = "Sin(";
            operacija = 5;
        }
        private void Zbroji_Click(object sender, EventArgs e)
        {
            Double.TryParse(textBox1.Text, out A);
            operacija = 1;
            textBox1.Clear();
            label1.Text = A.ToString() + "+";
        }
        private void button9_Click(object sender, EventArgs e)
        {
            Double.TryParse(textBox1.Text, out B);
            textBox1.Clear();
            Izracunaj();
            A = 0;
            B = 0;
            operacija = 0;
        }

        private void button5_Click(object sender, EventArgs e)
        {

            label1.Text = "cos(";
            operacija = 6;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            label1.Text = "ln(";
            operacija = 7;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            label1.Text = "log10(";
            operacija = 8;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Double.TryParse(textBox1.Text, out A);
            operacija = 9;
            textBox1.Clear();
            label1.Text = A.ToString() + "^";
        }
    }
}
